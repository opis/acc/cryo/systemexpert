<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2023-10-24 15:29:52 by emilioasensiconejero-->
<display version="2.0.0">
  <name>PV_VALVE_BlockIcon_Vertical_Left_Compact</name>
  <width>170</width>
  <height>75</height>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_1</name>
    <width>170</width>
    <height>74</height>
    <line_width>0</line_width>
    <background_color>
      <color red="112" green="115" blue="114" alpha="60">
      </color>
    </background_color>
    <corner_width>6</corner_width>
    <corner_height>6</corner_height>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Forced</name>
    <x>2</x>
    <y>25</y>
    <width>20</width>
    <height>22</height>
    <line_width>0</line_width>
    <background_color>
      <color name="LED-YELLOW-ON" red="255" green="235" blue="17">
      </color>
    </background_color>
    <rules>
      <rule name="Visibility rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Forced</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_AUTMANIcon</name>
    <text>E</text>
    <x>2</x>
    <y>21</y>
    <width>20</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="18.0">
      </font>
    </font>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>A</value>
        </exp>
        <exp bool_exp="pv1 == true">
          <value>M</value>
        </exp>
        <exp bool_exp="pv2 == true">
          <value>F</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Auto</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Manual</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:OpMode_Forced</pv_name>
      </rule>
    </rules>
    <tooltip>Opmode indicator</tooltip>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>${WIDDev}-${WIDIndex}</text>
    <x>70</x>
    <width>90</width>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="18.0">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>Device name</tooltip>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_CenterIcon_1</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:ValveColor</pv_name>
    <symbols>
      <symbol>../symbols/2-Way_Solenoid_Valve_Close.svg</symbol>
      <symbol>../symbols/2-Way_Solenoid_Valve_MAJOR-ERROR.svg</symbol>
      <symbol>../symbols/2-Way_Solenoid_Valve_Open_GREEN.svg</symbol>
    </symbols>
    <x>24</x>
    <y>14</y>
    <width>45</width>
    <height>45</height>
    <disconnect_overlay_color>
      <color red="149" green="110" blue="221" alpha="0">
      </color>
    </disconnect_overlay_color>
    <actions execute_as_one="true">
    </actions>
    <tooltip>Open faceplate</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <fallback_symbol>../../../_Symbols_SVG/2-Way_Solenoid_Valve/2-Way_Solenoid_Valve_INVALID.svg</fallback_symbol>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_Interlock_1</name>
    <symbols>
      <symbol>../../../CommonSymbols/interlock/Interlock_inactive.svg</symbol>
      <symbol>../../../CommonSymbols/interlock/Interlock_yellow.svg</symbol>
    </symbols>
    <x>1</x>
    <y>48</y>
    <width>24</width>
    <height>24</height>
    <actions>
    </actions>
    <rules>
      <rule name="Picture" prop_id="initial_index" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>0</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>1</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupInterlock</pv_name>
      </rule>
    </rules>
    <tooltip>Interlock event occured!</tooltip>
    <fallback_symbol>../../../_Symbols_SVG/CommonSymbols/interlock/Interlock_invalid.svg</fallback_symbol>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon_1</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupAlarm</pv_name>
    <symbols>
      <symbol>../../../CommonSymbols/error/error_inactive.svg</symbol>
      <symbol>../../../CommonSymbols/error/error_red.svg</symbol>
    </symbols>
    <width>24</width>
    <height>24</height>
    <disconnect_overlay_color>
      <color red="149" green="110" blue="221" alpha="0">
      </color>
    </disconnect_overlay_color>
    <actions>
    </actions>
    <tooltip>Alarm event occured!</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <fallback_symbol>../../../_Symbols_SVG/CommonSymbols/error/error_invalid.svg</fallback_symbol>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_Value_MV</name>
    <pv_name>Spk-110Crm:Cryo-CV-090:FB_OpenSP</pv_name>
    <x>70</x>
    <y>48</y>
    <width>92</width>
    <height>19</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="15.0">
      </font>
    </font>
    <precision>1</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt; 0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>${PLCName}:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
    <tooltip>Actual position</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_color>
      <color name="GROUP-BORDER" red="150" green="155" blue="151">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>Open SP:</text>
    <x>70</x>
    <y>32</y>
    <width>70</width>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="12.0">
      </font>
    </font>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>WID_OpenFaceplate_1</name>
    <actions>
      <action type="open_display">
        <file>../faceplates/SCV_VALVE_Faceplate.bob</file>
        <macros>
          <Dev>CV</Dev>
          <Dis>${WIDDis}</Dis>
          <Index>${WIDIndex}</Index>
          <SecSub>${WIDSecSub_Crm}</SecSub>
        </macros>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text></text>
    <width>170</width>
    <height>75</height>
    <transparent>true</transparent>
    <tooltip>Open faceplate</tooltip>
  </widget>
</display>
